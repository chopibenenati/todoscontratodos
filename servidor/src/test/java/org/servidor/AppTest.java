package org.servidor;

import edu.cei.tct.common.modelo.Usuario;
import edu.cei.tct.servidor.services.UsuarioService;
import edu.cei.tct.servidor.services.UsuarioServiceImpl;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple App.
 */
public class AppTest extends TestCase {
	/**
	 * Create the test case
	 *
	 * @param testName
	 *            name of the test case
	 */
	public AppTest(String testName) {
		super(testName);
	}

	/**
	 * @return the suite of tests being tested
	 */
	public static Test suite() {
		return new TestSuite(AppTest.class);
	}

	public void testUsuarioNotNull() {
		UsuarioService usuarioService = UsuarioServiceImpl.getInstance();
		Usuario usuario = usuarioService.buscarUsuario(new Usuario(null, "pepe1", "pepe1"));
		assertNotNull(usuario);
	}

	public void testUsuarioNull() {
		UsuarioService usuarioService = UsuarioServiceImpl.getInstance();
		Usuario usuario = usuarioService.buscarUsuario(new Usuario(null, "pepe1", "pepe2"));
		assertNull(usuario);
	}
}
