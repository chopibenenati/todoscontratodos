package interfaces;

import java.util.List;

import edu.cei.tct.common.modelo.Pais;
import edu.cei.tct.common.modelo.Usuario;

public interface Objetivo{

	public boolean completado(List<Pais> paises,Usuario usuario);
}
