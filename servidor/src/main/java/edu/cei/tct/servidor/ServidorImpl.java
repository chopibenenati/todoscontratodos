package edu.cei.tct.servidor;

import java.awt.EventQueue;
import java.rmi.RemoteException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimerTask;

import edu.cei.tct.common.modelo.Usuario;
import edu.cei.tct.common.servidor.Accion;
import edu.cei.tct.common.servidor.Cliente;
import edu.cei.tct.common.servidor.Comando;
import edu.cei.tct.common.servidor.Servidor;
import edu.cei.tct.common.servidor.controllers.LoginController;
import edu.cei.tct.common.servidor.controllers.PartidaController;

public class ServidorImpl extends Thread implements Servidor {

	private static final ServidorImpl instance = new ServidorImpl();

	private Map<String, Cliente> clientes = new HashMap<>();

	public static ServidorImpl getInstance() {
		return instance;
	}

	private ServidorImpl() {
		Thread t = new Thread(this, "Data Thread");
		t.start();
	}

	public void run() {
		while (true) {
			try {
				Thread.sleep(3000);
				System.out.println("Corriendo!!!!");
				PartidaControllerImpl.getInstance();
				if (clientes.size() >= 1 && !PartidaControllerImpl.isPartidaIniciada()) {
					PartidaControllerImpl.getInstance().iniciarPartida();
				}
				System.out.println(clientes.size());
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void registrarCliente(Cliente cliente, String id) throws RuntimeException {
		this.clientes.put(id, cliente);
		Comando comando = new Comando();
		comando.setAccion(Accion.USUARIO_LOGGEADO);
		notificar(comando);
	}

	@Override
	public LoginController getLoginController() throws RemoteException {
		return LoginControllerImpl.getInstance();
	}
	
	@Override
	public PartidaController getPartidaController() throws RemoteException {
		return PartidaControllerImpl.getInstance();
	}

	public void notificar(Comando comando) {
		clientes.forEach((k, v) -> {
			notificar(v, comando);
		});
	}

	public void notificar(Cliente cliente, Comando comando) {
		try {
			cliente.recibirComando(comando);
		} catch (RemoteException e) {
			e.printStackTrace();
		}
	}
}
