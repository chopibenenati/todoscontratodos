package edu.cei.tct.servidor.services;

import java.util.Arrays;

import clases.Dado;

public class DadoService {

	private int[] atacante;
	private int[] defensa;
	
	private static DadoService instance;
	
	public static DadoService getInstance() {
		
		if (instance == null) {
			
			instance = new DadoService();
		
		}
		return instance;
		
	}
	
	
	/**
	 * 
	 * @param cantidadDadosAtacante
	 * @param cantidadDadosDefensa
	 * @return matrix de dados primer conjunto dados atacante, segundo conjunto dados defensa
	 */
	public int[][] tirarDados(short cantidadDadosAtacante, short cantidadDadosDefensa) {
		atacante = new int[cantidadDadosAtacante];
		for (int i = 0; i < cantidadDadosAtacante; i++) {
			atacante[i] = Dado.getInstance().roll();
		}
		
		for (int i = 0; i < cantidadDadosDefensa; i++) {
			defensa[i] = Dado.getInstance().roll();
		}
		
		Arrays.sort(atacante);
		
		Arrays.sort(defensa);
		
		int[][] dados = new int[2][3];
		dados[0] = atacante;
		dados[1] = defensa;
	
		return dados;
		
	}
	
	/**
	 * La primera posicion es la cantidad ganada y el segundo la cantidad perdida
	 * @param atacante
	 * @param defensa
	 * @return
	 */
	public int[] compararDados(int[] atacante, int[] defensa){
		
		int[] ganadas = new int[2];
		int[] arr = defensa;
		ganadas[0] = ganadas[1] = 0;
		if(atacante.length > defensa.length) 
			arr = atacante;
			
			for (int i = 0; i < arr.length; i++) {
				if(defensa[i] > atacante[i]) {
					ganadas[1]++;
				}else {
					ganadas[0]++;
				}
			}
			
			return ganadas;
				
	}
	
	
	
	
}
