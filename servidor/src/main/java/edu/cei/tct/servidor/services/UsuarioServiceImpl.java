package edu.cei.tct.servidor.services;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import edu.cei.tct.common.modelo.Usuario;

public class UsuarioServiceImpl implements UsuarioService {

	private static final List<Usuario> usuarios = new LinkedList<>();
	private static final UsuarioServiceImpl instance = new UsuarioServiceImpl();
	
	public static UsuarioService getInstance() {
		return UsuarioServiceImpl.instance;
	}
	
	private UsuarioServiceImpl() {
		usuarios.add(new Usuario(1, "pepe1", "pepe1"));
		usuarios.add(new Usuario(2, "pepe2", "pepe2"));
		usuarios.add(new Usuario(3, "pepe3", "pepe3"));
		usuarios.add(new Usuario(4, "pepe4", "pepe4"));
		usuarios.add(new Usuario(5, "pepe5", "pepe5"));
		usuarios.add(new Usuario(6, "pepe6", "pepe6"));
		usuarios.add(new Usuario(7, "pepe7", "pepe7"));
	}

	@Override
	public Usuario buscarUsuario(Usuario usuario) {
		Usuario usuarioRegistrado = null;
		Iterator<Usuario> iterador = usuarios.iterator();
		while (iterador.hasNext() && usuarioRegistrado == null) {
			Usuario usuarioAux = iterador.next();
			// TODO cambiar la implementacion del equals
			// Da problemas dependiendo de como se use
			if (usuarioAux.equals(usuario)) {
				usuarioRegistrado = usuarioAux;
			}
		}
		return usuarioRegistrado;
	}
}
