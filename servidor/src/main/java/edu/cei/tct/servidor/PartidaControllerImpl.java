package edu.cei.tct.servidor;

import java.io.Serializable;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.LinkedList;
import java.util.List;

import clases.Objetivo6Paises;
import edu.cei.tct.common.modelo.Pais;
import edu.cei.tct.common.modelo.Usuario;
import edu.cei.tct.common.servidor.Accion;
import edu.cei.tct.common.servidor.Comando;
import edu.cei.tct.common.servidor.controllers.PartidaController;
import edu.cei.tct.servidor.services.PaisServiceImpl;
import interfaces.Objetivo;

public class PartidaControllerImpl extends UnicastRemoteObject implements PartidaController, Serializable {

	private static PartidaControllerImpl instance;
	private static boolean partidaIniciada = false;
	private static Usuario usuarioEnTurno;
	private static Objetivo objetivo;

	private static List<Usuario> usuariosEnPartida; // Lista de usuarios en la partida

	public static PartidaControllerImpl getInstance() throws RemoteException {

		if (instance == null) {

			instance = new PartidaControllerImpl();
			setObjetivo(new Objetivo6Paises());

		}
		return instance;

	}

	public Usuario getUsuarioEnTurno() {
		return PartidaControllerImpl.usuarioEnTurno;
	}

	protected PartidaControllerImpl() throws RemoteException {
		PartidaControllerImpl.usuariosEnPartida = new LinkedList<>();
	}

	public void iniciarPartida() throws RemoteException {
		PartidaControllerImpl.setPartidaIniciada(true);
		System.out.println("Envio mostrar mapa");
		Comando command = new Comando();
		command.setAccion(Accion.MOSTRAR_MAPA);
		ServidorImpl.getInstance().notificar(command);
		PartidaControllerImpl.usuariosEnPartida.addAll(LoginControllerImpl.getInstance().getUsuariosEnEspera());
		LoginControllerImpl.getInstance().getUsuariosEnEspera().clear();
		this.repartirTerritoriosInicialmente();
	}

	public void pasarPartida() {
		int index = PartidaControllerImpl.usuariosEnPartida.indexOf(usuarioEnTurno);
		if (usuariosEnPartida.size() < index++) {
			PartidaControllerImpl.usuarioEnTurno = usuariosEnPartida.get(0);
		} else {
			PartidaControllerImpl.usuarioEnTurno = usuariosEnPartida.get(index++);
		}
	}

	public void moverTropas(Pais paisDesde, Pais paisHacia, int tropasAMover) {

		Usuario usuarioJugando=getUsuarioEnTurno();
		if(PaisServiceImpl.getInstance().esPaisVecino(paisDesde, paisHacia)==true) {
			if(PaisServiceImpl.getInstance().tieneMismoUsuario(paisDesde, paisHacia)==true) {
				if(paisDesde.getJugador().getId()==usuarioJugando.getId()) {
					int cantidadTropasPosibles= paisDesde.getCantTropas()-1;
					if(cantidadTropasPosibles>=tropasAMover) {
						paisDesde.setCantTropas(cantidadTropasPosibles-tropasAMover);
						int cantidadTropasHacia=paisHacia.getCantTropas();
						paisHacia.setCantTropas(cantidadTropasHacia+tropasAMover);
					}else {
						System.out.println("no tienes esa cantidad de tropas para mover");
					}
					
				}
			}
		}
		
	}

	public void repartirTerritoriosInicialmente() {
		PaisServiceImpl.getInstance().distribuirTerritorios(usuariosEnPartida);
		Comando command = new Comando();
		command.setAccion(Accion.ACTUALIZAR_PAISES);
		ServidorImpl.getInstance().notificar(command);
	}

	@Override
	public List<Pais> getPaises() throws RemoteException {
		return PaisServiceImpl.getInstance().getPaises();
	}

	public static boolean isPartidaIniciada() {
		return partidaIniciada;
	}

	public static void setPartidaIniciada(boolean partidaIniciada) {
		PartidaControllerImpl.partidaIniciada = partidaIniciada;
	}
	
	public static Objetivo getObjetivo() {
		return objetivo;
	}

	public static void setObjetivo(Objetivo objetivo) {
		PartidaControllerImpl.objetivo = objetivo;
	}

}
