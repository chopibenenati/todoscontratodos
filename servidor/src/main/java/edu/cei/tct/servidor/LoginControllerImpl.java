package edu.cei.tct.servidor;

import java.io.Serializable;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.LinkedList;
import java.util.List;

import edu.cei.tct.common.modelo.Usuario;
import edu.cei.tct.common.servidor.controllers.LoginController;
import edu.cei.tct.servidor.services.UsuarioServiceImpl;

public class LoginControllerImpl extends UnicastRemoteObject implements LoginController, Serializable {
	private static List<Usuario> usuariosEnEspera;
	private static LoginControllerImpl instance;

	public static LoginControllerImpl getInstance() throws RemoteException {
		if (instance == null) {
			instance = new LoginControllerImpl();
		}
		return instance;
	}

	protected LoginControllerImpl() throws RemoteException {
		super();
		usuariosEnEspera = new LinkedList<>();
	}

	@Override
	public Usuario login(Usuario usuario) throws RemoteException {
		Usuario usuarioRegistrado = UsuarioServiceImpl.getInstance().buscarUsuario(usuario);
		usuariosEnEspera.add(usuarioRegistrado);
		return usuarioRegistrado;
	}

	@Override
	public List<Usuario> getUsuariosEnEspera() throws RemoteException {
		return this.usuariosEnEspera;
	}
}
