package edu.cei.tct.servidor.services;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import edu.cei.tct.common.modelo.Pais;
import edu.cei.tct.common.modelo.Usuario;

public class PaisServiceImpl implements PaisService{

	private final List<Pais> paises = new LinkedList<>();

	private static final PaisService instance = new PaisServiceImpl();

	public static PaisService getInstance() {
		return PaisServiceImpl.instance;
	}

	private PaisServiceImpl() {
		Pais AmericaNorte = new Pais();
		AmericaNorte.setIdPais(1);
		AmericaNorte.setGroupId("g26606");

		Pais Brasil = new Pais();
		Brasil.setIdPais(2);
		Brasil.setGroupId("g12135");
		
		Pais Uruguay = new Pais();
		Uruguay.setIdPais(3);
		Uruguay.setGroupId("g12234");
		
		Pais AfricaDelSur = new Pais();
		AfricaDelSur.setIdPais(4);
		AfricaDelSur.setGroupId("g12060");
		
		Pais AfricaDelNorte = new Pais();
		AfricaDelNorte.setIdPais(5);
		AfricaDelNorte.setGroupId("g11876");
		
		Pais Europa = new Pais();
		Europa.setIdPais(6);
		Europa.setGroupId("g11570");
		
		Pais Rusia = new Pais();
		Rusia.setIdPais(7);
		Rusia.setGroupId("g11393");
		
		Pais Asia = new Pais();
		Asia.setIdPais(8);
		Asia.setGroupId("g11118");
		
		Pais Oceania = new Pais();
		Oceania.setIdPais(9);
		Oceania.setGroupId("g10777");
		
		// agrego vecinos - Uruguay
		List<Pais> VecinosUruguay = new LinkedList<>();
		VecinosUruguay.add(Brasil);
		VecinosUruguay.add(AfricaDelSur);
		VecinosUruguay.add(Oceania);
		Uruguay.setVecinos(VecinosUruguay);

		// agrego vecinos - Brasil
		List<Pais> VecinosBrasil = new LinkedList<>();
		VecinosBrasil.add(Uruguay);
		VecinosBrasil.add(AmericaNorte);
		VecinosBrasil.add(AfricaDelNorte);
		Brasil.setVecinos(VecinosBrasil);

		// agrego vecinos - America Norte
		List<Pais> VecinosAmericaNorte = new LinkedList<>();
		VecinosAmericaNorte.add(Brasil);
		VecinosAmericaNorte.add(Europa);
		VecinosAmericaNorte.add(Rusia);
		AmericaNorte.setVecinos(VecinosAmericaNorte);

		// agrego vecinos - Europa
		List<Pais> VecinosEuropa = new LinkedList<>();
		VecinosEuropa.add(AmericaNorte);
		VecinosEuropa.add(Rusia);
		VecinosEuropa.add(AfricaDelNorte);
		Europa.setVecinos(VecinosEuropa);

		// agrego vecinos - Rusia
		List<Pais> VecinosRusia = new LinkedList<>();
		VecinosRusia.add(Europa);
		VecinosRusia.add(Asia);
		VecinosRusia.add(AmericaNorte);
		Rusia.setVecinos(VecinosRusia);

		// agrego vecinos - Asia
		List<Pais> VecinosAsia = new LinkedList<>();
		VecinosAsia.add(Rusia);
		VecinosAsia.add(Oceania);
		VecinosAsia.add(AfricaDelNorte);
		Asia.setVecinos(VecinosAsia);

		// agrego vecinos - Africa del Norte
		List<Pais> VecinosAfricaNorte = new LinkedList<>();
		VecinosAfricaNorte.add(Asia);
		VecinosAfricaNorte.add(AfricaDelSur);
		VecinosAfricaNorte.add(Europa);
		AfricaDelNorte.setVecinos(VecinosAfricaNorte);

		// agrego vecinos - Africa del Sur
		List<Pais> VecinosAfricaSur = new LinkedList<>();
		VecinosAfricaSur.add(AfricaDelNorte);
		VecinosAfricaSur.add(Uruguay);
		VecinosAfricaSur.add(Oceania);
		AfricaDelSur.setVecinos(VecinosAfricaSur);

		// agrego vecinos - Oceania
		List<Pais> VecinosOceania = new LinkedList<>();
		VecinosOceania.add(AfricaDelSur);
		VecinosOceania.add(Asia);
		VecinosOceania.add(Uruguay);
		Oceania.setVecinos(VecinosOceania);

		// agrego a la lista de paises general
		paises.add(Oceania);
		paises.add(Europa);
		paises.add(AfricaDelNorte);
		paises.add(AfricaDelSur);
		paises.add(AmericaNorte);
		paises.add(Asia);
		paises.add(Rusia);
		paises.add(Uruguay);
		paises.add(Brasil);

	}

	// @Override
	public Pais buscarPaisPorId(Pais pais) {
		Pais PaisEncontrado = null;
		Iterator<Pais> iterador = paises.iterator();
		while (iterador.hasNext() && PaisEncontrado == null) {
			Pais PaisAux = iterador.next();
			if (PaisAux.equals(pais)) {
				PaisEncontrado = PaisAux;
			}
		}
		return PaisEncontrado;
	}

	// aca retorna true si el pais es vecino

	public boolean esPaisVecino(Pais pais1, Pais pais2) {
		boolean paisVecino = false;

		Iterator<Pais> iterador = pais1.getVecinos().iterator();
		while (iterador.hasNext() && paisVecino == false) {
			Pais PaisAux = iterador.next();
			if (PaisAux.equals(pais2)) {
				paisVecino = true;
			}
		}

		return paisVecino;

	}

	public boolean tieneMismoUsuario(Pais country, Pais country2) {
		if (country.getJugador().equals(country2.getJugador())) {
			return true;
		}
		return false;
	}

	public void distribuirTerritorios(List<Usuario> usuariosEnPartida) {
		Iterator<Usuario> usrIterador = usuariosEnPartida.iterator();
		for(Pais p: paises) {
			Usuario usr = usrIterador.next();
			p.setJugador(usr);
			if(!usrIterador.hasNext()) {
				usrIterador = usuariosEnPartida.iterator();
			}
			
		}
		
	}
	
	public List<Pais> getPaises(){
		return this.paises;
	}

}
