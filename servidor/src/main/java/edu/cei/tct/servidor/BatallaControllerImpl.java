package edu.cei.tct.servidor;

import java.rmi.RemoteException;

import edu.cei.tct.common.modelo.Pais;
import edu.cei.tct.common.modelo.Usuario;
import edu.cei.tct.common.servidor.controllers.BatallaController;
import edu.cei.tct.servidor.services.DadoService;
import edu.cei.tct.servidor.services.PaisService;
import edu.cei.tct.servidor.services.PaisServiceImpl;
import edu.cei.tct.servidor.services.UsuarioServiceImpl;

public class BatallaControllerImpl implements BatallaController{

	private static final BatallaControllerImpl instance = new BatallaControllerImpl();

	public static BatallaControllerImpl getInstance() {
		return instance;
	}
	
	public static boolean atacar(Pais atacante, Pais atacado) {
		Usuario usuarioEnTurno = null;
		try {
			usuarioEnTurno = PartidaControllerImpl.getInstance().getUsuarioEnTurno();
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if(!PaisServiceImpl.getInstance().esPaisVecino(atacante, atacado)) {
			System.out.println("No es vecino!!!");
			return false;
		}
		
		if(PaisServiceImpl.getInstance().tieneMismoUsuario(atacante, atacado) && usuarioEnTurno.equals(atacante.getJugador())) {
			System.out.println("Tiene el mismo usuario!!!");
			return false;
		}
		
		if(atacante.getCantTropas() <= 1) {
			System.out.println("Tiene muy pocas tropas!!!");
			return false;
		}
		
		short cantidadDadosAtacante = 0, cantidadDadosDefensa = 0;
		cantidadDadosAtacante = (short) (atacante.getCantTropas() > 3 ?  3 :  atacante.getCantTropas());
		
		cantidadDadosDefensa = (short) (atacado.getCantTropas() > 3 ?  3 :  atacado.getCantTropas());
		
		int[][] dados = DadoService.getInstance().tirarDados(cantidadDadosAtacante, cantidadDadosDefensa);
		
		int[] ganados = DadoService.getInstance().compararDados(dados[0], dados[1]);
		
		if(ganados[0] > 0)
			atacado.setCantTropas(atacado.getCantTropas() - ganados[0]);
		else
			atacante.setCantTropas(atacante.getCantTropas() - ganados[1] );
		
		
		if(atacado.getCantTropas() <= 0)
			return true;
		return false;
	}
	
}
