package edu.cei;

import java.rmi.AlreadyBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

import edu.cei.tct.common.servidor.Servidor;
import edu.cei.tct.servidor.ServidorImpl;

/**
 * Hello world!
 *
 */
public class App {
	public static void main(String[] args) throws RemoteException, AlreadyBoundException {
		System.setProperty("java.security.policy", "file:///java.policy");
		if (System.getSecurityManager() == null) {
			System.setSecurityManager(new SecurityManager());
		}
		System.out.println("Hello World!");
		LocateRegistry.createRegistry(1099);
		Servidor srv = ServidorImpl.getInstance();
		Servidor stub = (Servidor) UnicastRemoteObject.exportObject(srv, 0);
		Registry registry = LocateRegistry.getRegistry(1099);
		registry.bind("server", stub);
		System.out.println("server ready");
	}
}
