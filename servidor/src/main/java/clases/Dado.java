package clases;

public class Dado {

	

	private static Dado instance;
	public static Dado getInstance() {
		
		if (instance == null) {
			
			instance = new Dado();
		
		}
		return instance;
		
	}
	
	public int roll() {
		return (int) (Math.random() * 6) + 1;
	}

}
