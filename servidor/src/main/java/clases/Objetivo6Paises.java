package clases;

import java.util.Iterator;
import java.util.List;

import org.junit.internal.runners.model.EachTestNotifier;

import edu.cei.tct.common.modelo.Pais;
import edu.cei.tct.common.modelo.Usuario;
import interfaces.Objetivo;

public class Objetivo6Paises implements Objetivo{

	@Override
	public boolean completado(List<Pais> paises, Usuario usuario) {
		// nuestro objetivo es conquistar 6 paises
		boolean respuesta=false;
		int contador=0;
		for (Iterator iterator = paises.iterator(); iterator.hasNext();) {
			Pais pais = (Pais) iterator.next();
			if(pais.getJugador().equals(usuario)) {
				contador++;
			}
			
		}
		//aca teoricamente se debería comparar solo = por que nunca va a ser mayor por que ya deberías haber ganado antes pero lo dejamos igual por las dudas
		if(contador>=6) {
			respuesta=true;
		}
		
		return respuesta;
	}

	
}
