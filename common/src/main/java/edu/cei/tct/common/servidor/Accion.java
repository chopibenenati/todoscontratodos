package edu.cei.tct.common.servidor;

public enum Accion {
	USUARIO_LOGGEADO, MOSTRAR_MAPA, ACTUALIZAR_PAISES;
}
