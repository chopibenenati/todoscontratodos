package edu.cei.tct.common.servidor.controllers;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

import edu.cei.tct.common.modelo.Pais;
import edu.cei.tct.common.modelo.Usuario;

public interface PartidaController extends Remote {

	List<Pais> getPaises() throws RemoteException;
	
	
	

}
