package edu.cei.tct.common.modelo;

import java.io.Serializable;
import java.util.List;

public class Pais implements Serializable {

	private int idPais;
	private Usuario jugador;
	private int cantTropas = 1;
	private String groupId;
	private List<Pais> vecinos;

	public int getIdPais() {
		return idPais;
	}

	public void setIdPais(int idPais) {
		this.idPais = idPais;
	}

	public int getCantTropas() {
		return cantTropas;
	}

	public void setCantTropas(int cantTropas) {
		this.cantTropas = cantTropas;
	}

	public List<Pais> getVecinos() {
		return vecinos;
	}

	public void setVecinos(List<Pais> vecinos) {
		this.vecinos = vecinos;
	}

	public Usuario getJugador() {
		return jugador;
	}

	public void setJugador(Usuario jugador) {
		this.jugador = jugador;
	}

	public String getGroupId() {
		return groupId;
	}

	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

}
