package edu.cei.tct.common.servidor.controllers;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

import edu.cei.tct.common.modelo.Usuario;
import edu.cei.tct.common.servidor.Cliente;

public interface LoginController extends Remote {

	public Usuario login(Usuario usuario) throws RemoteException;

	public List<Usuario> getUsuariosEnEspera() throws RemoteException;
	
}
