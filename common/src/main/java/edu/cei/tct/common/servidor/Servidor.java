package edu.cei.tct.common.servidor;

import java.rmi.Remote;
import java.rmi.RemoteException;

import edu.cei.tct.common.modelo.Usuario;
import edu.cei.tct.common.servidor.controllers.LoginController;
import edu.cei.tct.common.servidor.controllers.PartidaController;

public interface Servidor extends Remote {
	/**
	 * 
	 * @param cliente
	 * @param id
	 * @throws RemoteException
	 */
	public void registrarCliente(Cliente cliente, String id) throws RemoteException;

	/**
	 * 
	 * @return
	 * @throws RemoteException
	 */
	public LoginController getLoginController() throws RemoteException;

	public PartidaController getPartidaController() throws RemoteException;
}
