package edu.cei.tct.common.servidor;

import java.io.Serializable;

import edu.cei.tct.common.modelo.Usuario;

public class Comando implements Serializable {

	private Accion accion; // ataca, tira, ingresa, mueve
	private Usuario usuario;
	private int value;

	public Accion getAccion() {
		return accion;
	}

	public void setAccion(Accion accion) {
		this.accion = accion;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

}
