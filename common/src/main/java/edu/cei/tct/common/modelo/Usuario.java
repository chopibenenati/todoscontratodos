package edu.cei.tct.common.modelo;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Usuario implements Serializable {

	private Integer id;
	private String username;
	private String password;

	public Usuario() {
	}

	public Usuario(Integer id, String username, String password) {
		super();
		this.id = id;
		this.username = username;
		this.password = password;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return this.username;
	}
	
	@Override
	public boolean equals(Object obj) {
		boolean equals = false;
		if (obj instanceof Usuario) {
			Usuario usuario = (Usuario) obj;
			if (this.getUsername().equals(usuario.getUsername()) && 
					this.getPassword().equals(usuario.getPassword())) {
				equals = true;
			}
		}
		return equals;
	}
}
