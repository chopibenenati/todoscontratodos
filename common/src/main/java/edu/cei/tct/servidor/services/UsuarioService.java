package edu.cei.tct.servidor.services;

import edu.cei.tct.common.modelo.Usuario;

public interface UsuarioService {

	public Usuario buscarUsuario(Usuario usuario);

}
