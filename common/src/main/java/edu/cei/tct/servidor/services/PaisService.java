package edu.cei.tct.servidor.services;

import java.util.List;

import edu.cei.tct.common.modelo.Pais;
import edu.cei.tct.common.modelo.Usuario;

public interface PaisService {

	public List<Pais> getPaises();

	public void distribuirTerritorios(List<Usuario> usuariosEnPartida);

	public boolean tieneMismoUsuario(Pais atacante, Pais atacado);

	public boolean esPaisVecino(Pais atacante, Pais atacado);

}
