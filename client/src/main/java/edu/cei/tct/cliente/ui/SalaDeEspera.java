package edu.cei.tct.cliente.ui;

import java.awt.EventQueue;
import java.rmi.RemoteException;
import java.util.List;

import javax.swing.DefaultListModel;
import javax.swing.JFrame;

import edu.cei.tct.cliente.Estado;
import edu.cei.tct.cliente.WindowFlowManager;
import edu.cei.tct.cliente.conn.ClienteImpl;
import edu.cei.tct.common.modelo.Usuario;
import edu.cei.tct.common.servidor.Comando;
import javax.swing.SpringLayout;
import javax.swing.JList;

public class SalaDeEspera implements Window {

	private JFrame frame;
	private JList<Usuario> list;
	private DefaultListModel<Usuario> usuariosModel;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					SalaDeEspera window = new SalaDeEspera();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 * 
	 * @throws RemoteException
	 */
	public SalaDeEspera() throws RemoteException {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		SpringLayout springLayout = new SpringLayout();
		frame.getContentPane().setLayout(springLayout);
		usuariosModel = new DefaultListModel<>();
		list = new JList<Usuario>();
		list.setModel(usuariosModel);
		springLayout.putConstraint(SpringLayout.NORTH, list, 28, SpringLayout.NORTH, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.WEST, list, 48, SpringLayout.WEST, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.SOUTH, list, 231, SpringLayout.NORTH, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.EAST, list, 339, SpringLayout.WEST, frame.getContentPane());
		frame.getContentPane().add(list);
		
		
	}
	
	
	@Override
	public void mostrar() {
		this.frame.setVisible(true);
	}

	@Override
	public void recibirComando(Comando comando) {
		System.out.println("sala de espera");
		switch (comando.getAccion()) {
		case USUARIO_LOGGEADO:
			usuariosModel.clear();
			List<Usuario> usuarios;
			try {
				usuarios = ClienteImpl.getInstance().getListaDeUsuariosEnEspera();
				usuarios.forEach((v) -> {
					usuariosModel.addElement(v);
				});
			} catch (RemoteException e) {
				e.printStackTrace();
			}
			break;
		case MOSTRAR_MAPA:
			System.out.println("Aqui mostrar mapa?");
			try {
				WindowFlowManager.getInstance().change(Estado.MOSTRAR_MAPA);
			} catch (RemoteException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		default:
			break;
		}

	}

	@Override
	public void close() {
		this.frame.dispose();
	}
}
