package edu.cei.tct.cliente.ui;

public interface MainWindowObserver {

	public void update(String message);
	
}

