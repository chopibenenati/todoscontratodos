package edu.cei.tct.cliente;

import java.awt.EventQueue;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

import edu.cei.tct.cliente.conn.ClienteImpl;
import edu.cei.tct.cliente.ui.LoginWindow;
import edu.cei.tct.cliente.ui.MainWindow;
import edu.cei.tct.cliente.ui.SalaDeEspera;
import edu.cei.tct.cliente.ui.Window;
import edu.cei.tct.common.servidor.Comando;

public class WindowFlowManager {

	private static final WindowFlowManager instance = new WindowFlowManager();
	private Estado estado;

	private Window currentWindow;

	public static WindowFlowManager getInstance() {
		return instance;
	}

	private WindowFlowManager() {
	}

	public void start() {
		LoginWindow loginWindow = new LoginWindow();
		currentWindow = loginWindow;
		cambiarWindowActiva();
	}

	public void change(Estado estado) throws RemoteException {
		this.estado=estado;
		
		currentWindow.close();

		switch (estado) {
		case LOGIN:
			break;
		case SALA_DE_ESPERA:
			currentWindow = new SalaDeEspera();
			break;
		case MOSTRAR_MAPA:
			try {
				currentWindow = new MainWindow();
				System.out.println("Mostrando Mapa");
			} catch (NotBoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			break;
		default:
			break;
		}

		cambiarWindowActiva();

		if (estado == Estado.SALA_DE_ESPERA) {
			ClienteImpl.getInstance().registrarCliente();
		}
		
		
	}

	private void cambiarWindowActiva() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					currentWindow.mostrar();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public void enviarComando(Comando comando) {
		System.out.println("WFM");
		currentWindow.recibirComando(comando);
	}
}
