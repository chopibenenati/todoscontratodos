package edu.cei.tct.cliente;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;

/**
 * Hello world!
 *
 */
public class App {
	public static void main(String[] args) throws RemoteException, NotBoundException {
		WindowFlowManager.getInstance().start();
	}
}
