package edu.cei.tct.cliente.ui;

import java.awt.EventQueue;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import edu.cei.tct.cliente.Estado;
import edu.cei.tct.cliente.WindowFlowManager;
import edu.cei.tct.cliente.conn.ClienteImpl;
import edu.cei.tct.common.modelo.Pais;
import edu.cei.tct.common.modelo.Usuario;
import edu.cei.tct.common.servidor.Cliente;
import edu.cei.tct.common.servidor.Comando;
import javafx.embed.swing.JFXPanel;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import javax.swing.SpringLayout;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.awt.event.ActionEvent;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class MainWindow implements MainWindowObserver, Window {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private JFrame frame;
	private Cliente cliente;
    private JFXPanel fxPanel;
    MapSceneController mapSceneController;
    private JTextField inputTropas;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainWindow window = new MainWindow();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 * @throws RemoteException 
	 * @throws NotBoundException 
	 */
	public MainWindow() throws RemoteException, NotBoundException {
		// Cliente que conecta con el servidor
		this.cliente = ClienteImpl.getInstance();
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 1000, 610);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		SpringLayout springLayout = new SpringLayout();
		
		frame.getContentPane().setLayout(springLayout);
		
		this.fxPanel = new JFXPanel();
		FXMLLoader loader;
		try {
			loader = new FXMLLoader(getClass().getResource("MapScene.fxml"));
			Parent root = loader.load();
			Scene scene = new Scene(root);
			mapSceneController = loader.<MapSceneController>getController();
			mapSceneController.addObserver(this);
			this.fxPanel.setScene(scene);
			
		} catch (IOException e) {
			e.printStackTrace();
		}
        
		springLayout.putConstraint(SpringLayout.NORTH, this.fxPanel, 40, SpringLayout.NORTH, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.WEST, this.fxPanel, 0, SpringLayout.WEST, frame.getContentPane());
		frame.getContentPane().add(this.fxPanel);
		
		JButton btnNewButton = new JButton("New button");
		
		
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					cliente.enviarMensaje(String.valueOf(System.currentTimeMillis()));
				} catch (RemoteException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		springLayout.putConstraint(SpringLayout.NORTH, btnNewButton, 10, SpringLayout.NORTH, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.WEST, btnNewButton, 26, SpringLayout.WEST, frame.getContentPane());
		frame.getContentPane().add(btnNewButton);
		
		JLabel lblNewLabel = new JLabel("New label");
		springLayout.putConstraint(SpringLayout.NORTH, lblNewLabel, 0, SpringLayout.NORTH, btnNewButton);
		springLayout.putConstraint(SpringLayout.WEST, lblNewLabel, 69, SpringLayout.EAST, btnNewButton);
		frame.getContentPane().add(lblNewLabel);
		
		JButton btnAtacar = new JButton("Atacar");
		btnAtacar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		springLayout.putConstraint(SpringLayout.NORTH, btnAtacar, 15, SpringLayout.NORTH, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.WEST, btnAtacar, 11, SpringLayout.EAST, lblNewLabel);
		frame.getContentPane().add(btnAtacar);
		
		JButton btnMoverTropas = new JButton("Mover Tropas");
		btnMoverTropas.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		springLayout.putConstraint(SpringLayout.NORTH, btnMoverTropas, 0, SpringLayout.NORTH, btnAtacar);
		springLayout.putConstraint(SpringLayout.WEST, btnMoverTropas, 13, SpringLayout.EAST, btnAtacar);
		frame.getContentPane().add(btnMoverTropas);
		
		JButton btnPasarTurno = new JButton("Pasar Turno");
		btnPasarTurno.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		springLayout.putConstraint(SpringLayout.WEST, btnPasarTurno, 17, SpringLayout.EAST, btnMoverTropas);
		springLayout.putConstraint(SpringLayout.SOUTH, btnPasarTurno, 0, SpringLayout.SOUTH, btnAtacar);
		frame.getContentPane().add(btnPasarTurno);
		
		inputTropas = new JTextField();
		springLayout.putConstraint(SpringLayout.NORTH, inputTropas, 0, SpringLayout.NORTH, btnNewButton);
		springLayout.putConstraint(SpringLayout.EAST, inputTropas, -51, SpringLayout.EAST, frame.getContentPane());
		frame.getContentPane().add(inputTropas);
		inputTropas.setColumns(10);
		
		JButton btnColocarTropas = new JButton("Colocar Tropas");
		springLayout.putConstraint(SpringLayout.WEST, btnColocarTropas, 16, SpringLayout.EAST, btnPasarTurno);
		springLayout.putConstraint(SpringLayout.SOUTH, btnColocarTropas, 0, SpringLayout.SOUTH, btnAtacar);
		frame.getContentPane().add(btnColocarTropas);
		
		JLabel labelError = new JLabel("New label");
		springLayout.putConstraint(SpringLayout.NORTH, labelError, 0, SpringLayout.NORTH, fxPanel);
		springLayout.putConstraint(SpringLayout.EAST, labelError, -49, SpringLayout.WEST, btnAtacar);
		frame.getContentPane().add(labelError);
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public JFrame getFrame() {
		return frame;
	}
	
	@Override
	public void update(String message) {
		System.out.println("-----> 1");
		System.out.println(message);
	}

	@Override
	public void mostrar() {
		this.frame.setVisible(true);
	}

	@Override
	public void close() {
		this.frame.dispose();
		
	}
	



	@Override
	public void recibirComando(Comando comando) {
		System.out.println("Comando mil veces!!");
		switch (comando.getAccion()) {
		case ACTUALIZAR_PAISES:
			mapSceneController.recibirComando(comando);
			break;
		default:
			break;
		}
	}
}
