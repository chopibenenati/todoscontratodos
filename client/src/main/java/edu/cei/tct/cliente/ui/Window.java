package edu.cei.tct.cliente.ui;

import edu.cei.tct.common.servidor.Comando;

public interface Window {

	public void mostrar();
	public void close();
	public void recibirComando(Comando comando);
}
