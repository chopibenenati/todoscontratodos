package edu.cei.tct.cliente;

public enum Estado {
	LOGIN, SALA_DE_ESPERA, MAPA, ESTADISTICAS, ETC, MOSTRAR_MAPA, MOVIENDO_TROPAS;
}
