package edu.cei.tct.cliente.conn;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import edu.cei.tct.cliente.WindowFlowManager;
import edu.cei.tct.common.modelo.Pais;
import edu.cei.tct.common.modelo.Usuario;
import edu.cei.tct.common.servidor.Cliente;
import edu.cei.tct.common.servidor.Comando;
import edu.cei.tct.common.servidor.Servidor;
import edu.cei.tct.common.servidor.Window;

public class ClienteImpl extends UnicastRemoteObject implements Cliente {

	private Servidor servidor;
	private String id;
	private Queue<Comando> comandos;
	private UIProcessorThread uiProcessorThread;

	private static ClienteImpl instance;

	public static ClienteImpl getInstance() {
		try {
			if (ClienteImpl.instance == null) {
				ClienteImpl.instance = new ClienteImpl();
			}
		} catch (RemoteException | NotBoundException e) {
			e.printStackTrace();
		}
		return ClienteImpl.instance;
	}

	private ClienteImpl() throws RemoteException, NotBoundException {
		System.setProperty("java.security.policy", "file:///java.policy");
		if (System.getSecurityManager() == null) {
			System.setSecurityManager(new SecurityManager());
		}
		Registry registry = LocateRegistry.getRegistry(1099);
		this.servidor = (Servidor) registry.lookup("server");
		this.id = String.valueOf(System.currentTimeMillis());

		this.comandos = new LinkedList<>();
		this.uiProcessorThread = new UIProcessorThread();
		this.uiProcessorThread.start();
	}

	@Override
	public void recibirComando(Comando comando) {
		this.comandos.add(comando);
	}

	public void enviarMensaje(String mensaje) throws RemoteException {
	}

	@Override
	public Usuario login(Usuario usuario) throws RemoteException {
		Usuario usuarioLoggeado = this.servidor.getLoginController().login(usuario);
		return usuarioLoggeado;
	}
	
	public void registrarCliente() throws RemoteException {
		this.servidor.registrarCliente(this, id);
	}

	class UIProcessorThread extends Thread {
		@Override
		public void run() {
			super.run();
			while (true) {
				Comando comando = comandos.poll();
				if (comando != null) {
					System.out.println(comando.getAccion());
					WindowFlowManager.getInstance().enviarComando(comando);
				} else {
					try {
						Thread.sleep(100);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
		}
	}

	public List<Pais> getPaises() throws RemoteException {
		return servidor.getPartidaController().getPaises();
	}
	
	public List<Usuario> getListaDeUsuariosEnEspera() throws RemoteException {
		return servidor.getLoginController().getUsuariosEnEspera();
	}
}
