package edu.cei.tct.cliente.ui;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.rmi.RemoteException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.SpringLayout;

import edu.cei.tct.cliente.Estado;
import edu.cei.tct.cliente.WindowFlowManager;
import edu.cei.tct.cliente.conn.ClienteImpl;
import edu.cei.tct.common.modelo.Usuario;
import edu.cei.tct.common.servidor.Comando;

public class LoginWindow implements Window {

	private JFrame frame;
	private JTextField usernameField;
	private JPasswordField passwordField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					LoginWindow window = new LoginWindow();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public JFrame getFrame() {
		return frame;
	}

	/**
	 * Create the application.
	 */
	public LoginWindow() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		SpringLayout springLayout = new SpringLayout();
		frame.getContentPane().setLayout(springLayout);

		usernameField = new JTextField();
		springLayout.putConstraint(SpringLayout.WEST, usernameField, -295, SpringLayout.EAST, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.SOUTH, usernameField, -196, SpringLayout.SOUTH, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.EAST, usernameField, -81, SpringLayout.EAST, frame.getContentPane());
		frame.getContentPane().add(usernameField);
		usernameField.setColumns(10);

		JButton loginButton = new JButton("Login");
		loginButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Usuario usuario = new Usuario(null, usernameField.getText(), new String(passwordField.getPassword()));
				try {
					Usuario usuarioRegistrado = ClienteImpl.getInstance().login(usuario);
					if (usuarioRegistrado != null) {
						WindowFlowManager.getInstance().change(Estado.SALA_DE_ESPERA);
					}
				} catch (RemoteException e1) {
					e1.printStackTrace();
				}
			}
		});
		springLayout.putConstraint(SpringLayout.SOUTH, loginButton, -10, SpringLayout.SOUTH, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.EAST, loginButton, -10, SpringLayout.EAST, frame.getContentPane());
		frame.getContentPane().add(loginButton);

		JLabel lblUsername = new JLabel("Username");
		springLayout.putConstraint(SpringLayout.NORTH, lblUsername, 5, SpringLayout.NORTH, usernameField);
		springLayout.putConstraint(SpringLayout.EAST, lblUsername, -44, SpringLayout.WEST, usernameField);
		frame.getContentPane().add(lblUsername);

		JLabel lblPassword = new JLabel("Password");
		springLayout.putConstraint(SpringLayout.NORTH, lblPassword, 43, SpringLayout.SOUTH, lblUsername);
		springLayout.putConstraint(SpringLayout.EAST, lblPassword, 0, SpringLayout.EAST, lblUsername);
		frame.getContentPane().add(lblPassword);

		passwordField = new JPasswordField();
		springLayout.putConstraint(SpringLayout.NORTH, passwordField, 33, SpringLayout.SOUTH, usernameField);
		springLayout.putConstraint(SpringLayout.WEST, passwordField, 0, SpringLayout.WEST, usernameField);
		springLayout.putConstraint(SpringLayout.EAST, passwordField, 0, SpringLayout.EAST, usernameField);
		frame.getContentPane().add(passwordField);
	}

	@Override
	public void mostrar() {
		this.frame.setVisible(true);
	}

	@Override
	public void recibirComando(Comando comando) {
		// TODO Auto-generated method stub
		System.out.println("Login window");
	}

	@Override
	public void close() {
		this.frame.dispose();
	}
}
