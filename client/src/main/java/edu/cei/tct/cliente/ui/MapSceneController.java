/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.cei.tct.cliente.ui;

import java.net.URL;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.application.Platform;
import edu.cei.tct.cliente.conn.ClienteImpl;
import edu.cei.tct.common.modelo.Pais;
import edu.cei.tct.common.servidor.Comando;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Group;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;

import javafx.scene.paint.Color;

/**
 *
 */
public class MapSceneController implements Initializable {

	@FXML
	private Label americaNorte;
	@FXML
	private Label brasil;
	@FXML
	private Label uruguay;
	@FXML
	private Label africaDelNorte;
	@FXML
	private Label africaDelSur;
	@FXML
	private Label europa;
	@FXML
	private Label asia;

	private List<MainWindowObserver> observers;

	public MapSceneController() {
		this.observers = new ArrayList<>();
	}

	@FXML
	private Label rusia;
	@FXML
	private Label label;

	@FXML
	private Label oceania;

	private Label selected1;
	private Label selected2;

	@FXML
	private void handleButtonAction(MouseEvent event) {
		Group group = (Group) event.getSource();
		String groupid = group.getId().toString();
		System.out.println("Mouse click on label: " + groupid);
		Label clicked = returnLabel(groupid);

		if (selected1 != null && selected1.equals(clicked)) {
			selected1.setTextFill(Color.BLACK);
			selected1 = null;
		} else if (selected2 != null && selected2.equals(clicked)) {
			selected2.setTextFill(Color.BLACK);
			selected2 = null;
		} else if (selected1 != null) {
			selected2 = clicked;
			selected2.setTextFill(Color.BLUE);
		} else {
			selected1 = clicked;
			selected1.setTextFill(Color.BLUE);
		}

		// this.notifyObservers(label.toString());
	}

	public void updateMap() {
		System.out.println("H-------------------");
		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				MapSceneController.this.updateMapWithCountries();
			}
		});
	}

	@Override
	public void initialize(URL url, ResourceBundle rb) {

	}

	public void recibirComando(Comando command) {
		switch (command.getAccion()) {
		case ACTUALIZAR_PAISES:
			MapSceneController.this.updateMap();
			break;
		default:
			break;
		}
	}

	public void addObserver(MainWindowObserver observer) {
		this.observers.add(observer);
	}

	void updateMapWithCountries() {
		System.out.println("Update map with countries");
		List<Pais> paises;
		try {
			paises = ClienteImpl.getInstance().getPaises();
			for (Pais pais : paises) {
				System.out.println(pais.toString());
				System.out.println(pais.getGroupId());
				Label ps = returnLabel(pais.getGroupId());
				ps.setText(ps.getText()+": "+pais.getJugador().getUsername() + " Tropas: " + pais.getCantTropas() );

			}
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void notifyObservers(String message) {
		for (MainWindowObserver observer : this.observers) {
			observer.update(message);
		}
	}

	private Label returnLabel(String groupid) {
		switch (groupid) {
		case "g26606":
			return this.americaNorte;
		case "g12135":
			return this.brasil;
		case "g11876":
			return this.africaDelNorte;
		case "g12060":
			return this.africaDelSur;
		case "g11118":
			return this.asia;
		case "g11570":
			return this.europa;
		case "g11393":
			return this.rusia;
		case "g10777":
			return this.oceania;
		case "g12234":
			return this.uruguay;
		default:
			return null;
		}
	}

}
